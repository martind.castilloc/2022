#ifndef LISTA_H
#define LISTA_H

#include<string>
#include <iostream>

using namespace std;

class Estudiante{
    private: 
        string nombre;
        int edad;

    public:

        Estudiante(string nombre, int edad){
            this->nombre = nombre;
            this->edad = edad;
            
        }

        Estudiante(){
            this->nombre = "";
            this->edad = 0;
        }

        string getNombre(){
            return nombre;
        }

        void setNombre(string nombre){
            this->nombre = nombre;
        }   

        int getEdad(){
            return edad;
        }

        void setEdad(int edad){
            this->edad = edad;
        }
};

class NodoEstudiante{
    private:
        Estudiante valor;
        NodoEstudiante *vecino;

    public:
        NodoEstudiante(){
            this->vecino = NULL;
        }

        NodoEstudiante(Estudiante nuevo){
            this->valor.setNombre(nuevo.getNombre());
            this->valor.setEdad(nuevo.getEdad());

            this->vecino = NULL;
        }

        void setVecino(NodoEstudiante *vec){
            this->vecino = vec;
        }
                
        NodoEstudiante *getVecino(){
            return(this->vecino);
        }

        Estudiante *getEstudiante(){
            return(&this->valor);
        }

};

class  Lista{

    private:
        NodoEstudiante *primero, *ultimo;

    public:
        Lista(){
            primero = ultimo = NULL;
        }

        //Funcion que imprime la lista de postres.
        void mostrar_estudiantes();   
        //Funcion que elimina un postre.
        void EliminarPrimero();
        //Funcion para agregar los ingredientes.
        void buscarEstudiante(string dato);
        //Funcion para agregar a los alumnos.
        void insertarNodo(NodoEstudiante *nuevo);
        //Funcion para eliminar el ultmo elemento de la lista.
        void Eliminar_ultimo();

};
#endif
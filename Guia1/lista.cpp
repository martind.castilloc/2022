#include "lista.h"

using namespace std;


// Funcion que imprime la lista de los estudiantes.
void Lista::mostrar_estudiantes(){
    NodoEstudiante *recorrer;
    recorrer = primero;

    while(recorrer !=NULL){
        Estudiante *estudiante;
        estudiante = recorrer->getEstudiante();

        cout <<"Estudiante" << endl;
        cout <<"-- Nombre: " << estudiante->getNombre()  << endl;
        cout <<"-- Edad: " << estudiante->getEdad() << endl;

        recorrer = recorrer->getVecino();
    }
}

void Lista::EliminarPrimero(){
    if (primero!=NULL){
        NodoEstudiante *aux_primero;
        aux_primero = this->primero;
        this->primero = this->primero->getVecino();
        aux_primero->setVecino(nullptr);
        delete aux_primero;
        cout << "se ha eliminado el primer estudiante" << endl; 
    }else {
        cout <<"La lista de estudiantes está vacía"<<endl;
    }
} 

void Lista::buscarEstudiante(string dato){
            
    NodoEstudiante *actual = new NodoEstudiante();
    actual = primero;
    bool band = false;

    while (actual != NULL){
        Estudiante *estudiante;
        estudiante = actual->getEstudiante();
        if (estudiante->getNombre() == dato){
            band = true;
            cout << "\n el alumno ha sido encontrado" << endl;
        }
        actual = actual->getVecino();          
    }
    if (band == false){
        cout << "\n alumno no ha sido encontrado" << endl;
    }
}

void Lista::insertarNodo(NodoEstudiante *nuevo){
    if (ultimo == NULL){
        primero = ultimo = nuevo;
    }else{
        ultimo->setVecino(nuevo);
        ultimo = nuevo;
    }            
}

void Lista::Eliminar_ultimo(){
    if (primero!=NULL){
        NodoEstudiante *nuevo = this->primero;
        NodoEstudiante *aux_borrar = nullptr;
        while (true){
            if (primero!=ultimo){
                aux_borrar = nuevo;
                nuevo = nuevo->getVecino();
                if (nuevo==ultimo){
                    aux_borrar->setVecino(nullptr);
                    delete nuevo;
                    this->ultimo = aux_borrar;
                }
            }else {
                this->EliminarPrimero();
                break;
            }
        }
    }else {
        cout <<"La lista de estudiantes esta vacia" <<endl;
    }
}

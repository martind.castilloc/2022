#include "lista.h"

using namespace std;


string menu (string opc) {

    cout << "\n";
    cout << "\t     ---------------------------- " << endl;
	cout << "\t     ***** MENÚ DE OPCIONES ***** " << endl;
	cout << "\t     ---------------------------- " << endl;
	cout << "\n  [1]  > Agregar Estudiantes\t" << endl;
    cout << "  [2]  > Buscar Estudiante\t" << endl;
    cout << "  [3]  > Eliminar el primer estudiante\t" << endl;
    cout << "  [4]  > Eliminar el ultimo estudiante\t" << endl;
    cout << "  [5]  > Mostrar lista completa de estudiantes\t" << endl;
    cout << "  [6]  > Salir del programa\t" << endl;
    cout << "Opción: ";

    cin >> opc;
	cin.ignore();

    return opc;
}

int main(){  

    // Inicializa las variables para el menu
    string option = "\0";
    string nombre = "\0";
    int edad = 0;

    Lista lista;

    while (option != "6") {

        option = menu(option);

        //Opcion para aplicar el algoritmo de Prim.
        if (option == "1") {
            
            cout << "\n ingrese el nombre del estudiante: "<< endl;
            cin >> nombre;
            cout << "\n ingrese la edad del estudiante: "<< endl;
            cin >> edad;         
            Estudiante p(nombre, edad);
            NodoEstudiante *nuevo= new NodoEstudiante(p);
            lista.insertarNodo(nuevo);
                
        //Opción para crear y visualizar el grafo con la librería graphviz.
        }else if (option == "2") {
            cout << "\n ingrese el nombre a buscar: "<< endl;
            cin >> nombre;
            lista.buscarEstudiante(nombre);   
        } 

        else if (option == "3") {
            lista.EliminarPrimero();   
        } 

        else if (option == "4"){
            lista.Eliminar_ultimo();
        }

        else if (option == "5"){
            cout << "\n";
            lista.mostrar_estudiantes();
        }
    }

    return 0;
}
